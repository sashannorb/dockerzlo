FROM debian:9

RUN apt update && apt install -y wget gcc make nano && apt update &&  apt install -y libpcre3 libpcre3-dev zlib1g-dev
RUN wget http://nginx.org/download/nginx-1.12.0.tar.gz  && tar xvfz nginx-1.12.0.tar.gz && cd nginx-1.12.0 && ./configure && make && make install
CMD ["/usr/local/nginx/sbin/nginx", "-g", "daemon off;"]
